//  Copyright 2012 happy_ryo
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#import "HRViewController.h"
#import "HRTestViewController.h"
#import "UIViewController+HRModalViewController.h"

@interface HRViewController ()

@end

@implementation HRViewController {
    HRTestViewController *_hrTestViewController;
    UIButton *_openBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _hrTestViewController = [[HRTestViewController alloc] init];
    _openBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _openBtn.frame = CGRectMake(0, 0, 70, 50);
    _openBtn.center = self.view.center;
    [_openBtn setTitle:@"OPEN" forState:UIControlStateNormal];
    [_openBtn addTarget:self action:@selector(presentTestModalView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_openBtn];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.8f];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        _openBtn.frame = CGRectMake(205, 125, 70, 50);
    } else {
        _openBtn.frame = CGRectMake(125, 205, 70, 50);
    }
    [UIView commitAnimations];
}


- (void)presentTestModalView {
    [self presentHRModalViewController:_hrTestViewController];
}

@end
