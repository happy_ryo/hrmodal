//  Copyright 2012 happy_ryo
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
#import "UIViewController+HRModalViewController.h"
#import "HRModalViewController.h"


@implementation UIViewController (HRModalViewController)

- (void)presentHRModalViewController:(HRModalViewController *)viewController {
    UIView *modalView = viewController.view;
    UIView *coverView = viewController.coverView;
    CGPoint center = self.view.center;

    CGPoint presentStartCenter;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    UIInterfaceOrientation uiInterfaceOrientation = (UIInterfaceOrientation) [[UIDevice currentDevice] orientation];

    if (uiInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || uiInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        presentStartCenter = CGPointMake(screenSize.height / 2.0f, screenSize.width * 1.3f);
        center = CGPointMake(center.y, center.x);
        coverView.frame = CGRectMake(0, 0, 480, 300);
    } else {
        presentStartCenter = CGPointMake(screenSize.width / 2.0f, screenSize.height * 1.3f);
        coverView.frame = CGRectMake(0, 0, 320, 460);
    }
    modalView.center = presentStartCenter;
    modalView.bounds = self.view.bounds;

    coverView.alpha = 0.0f;

    [self.view addSubview:coverView];
    [self.view addSubview:modalView];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:PRESENT_ANIMATION_DURATION];

    modalView.center = center;
    coverView.alpha = COVER_VIEW_ALPHA;

    [UIView commitAnimations];
}

- (void)dismissHRModalViewController:(HRModalViewController *)viewController {
    UIView *modalView = viewController.view;
    UIView *coverView = viewController.coverView;

    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGPoint dismissEndCenter;

    UIInterfaceOrientation orientation = (UIInterfaceOrientation) [[UIDevice currentDevice] orientation];
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        dismissEndCenter = CGPointMake(screenSize.height / 2.0f, screenSize.width * 1.3f);
    } else {
        dismissEndCenter = CGPointMake(screenSize.width / 2.0f, screenSize.height * 1.3f);
    }

    [UIView beginAnimations:nil context:(__bridge void*)modalView];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(dismissHRModalViewControllerEnded:finished:context:)];
    [UIView setAnimationDuration:DISMISS_ANIMATION_DURATION];

    modalView.center = dismissEndCenter;
    coverView.alpha = 0.0f;

    [UIView commitAnimations];

    [coverView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:DISMISS_ANIMATION_DURATION];
}

- (void)dismissHRModalViewControllerEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    UIView *modalView = (__bridge UIView *) context;
    [modalView removeFromSuperview];
}
@end