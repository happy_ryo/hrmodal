//  Copyright 2012 happy_ryo
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
#import "HRTestViewController.h"
#import "UIViewController+HRModalViewController.h"


@interface HRTestViewController ()
- (void)dismissHRModalViewController;

@end

@implementation HRTestViewController {
    UIPickerView *_pickerView;
    UIButton *_closeBtn;
}

- (id)init {
    self = [super init];
    if (self) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.center = self.view.center;
        _pickerView.backgroundColor = [UIColor blueColor];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        _pickerView.frame = CGRectMake(0, 244, 320, 216);
        [self.view addSubview:_pickerView];

        _closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_closeBtn addTarget:self action:@selector(dismissHRModalViewController) forControlEvents:UIControlEventTouchUpInside];
        _closeBtn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _closeBtn.frame = CGRectMake(0, 200, 320, 44);
        [_closeBtn setTitle:@"CLOSE" forState:UIControlStateNormal];
        [self.view addSubview:_closeBtn];
    }

    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)dismissHRModalViewController {
    [self dismissHRModalViewController:self];
}

#pragma mark picker view data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 4;
}

#pragma mark picker view delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSInteger titleInt = row + 1;
    return [NSString stringWithFormat:@"%li", titleInt];
}

@end